#!/bin/bash

echo $1

data=$(zenity --forms  --title="Class creation."\
 --add-entry="Class Name" \
 --add-entry="Namespace" );


case $? in
     1) echo "you cancelled"; exit 1 ;;
    -1) echo "some error occurred"; exit -1 ;;
     0) IFS="|" read -r className namespace <<< "$data" ;;
esac


header="#ifndef _H_${className}\n
#define _H_${className}\n
\n
namespace ${namespace}\n
{\n
\tclass ${className}\n
\t{\n
\tpublic:\n
\t\t${className}();\n
\t\tvirtual ~${className}();\n
\t};\n
\n
}\n
#endif"

src="#include \"${className}.hpp\"\n
\n
namespace ${namespace}\n
{\n
\t	${className}::${className}()\n
\t	{\n
\t\n
\t	}\n
\t\n
\t	${className}::~${className}()\n
\t	{\n
\t\n
\t	}\n
}\n"

echo -e ${header} >> ${className}.hpp
echo -e ${src} >> ${className}.cpp