import sublime
import sublime_plugin
import os

USER = os.getenv('USER')

class AddcppclassCommand(sublime_plugin.WindowCommand):
	def run(self, dirs, pkgpath):
		self.window.run_command("exec", {
			"cmd": ["/home/"+USER+"/.config/sublime-text-3/Packages/AddCppClass/AddCppClass.sh", dirs[0]],
			"shell": False
			})

# Make command visible only when there is a single file
	def is_visible(self, dirs, pkgpath):
		return len(dirs) == 1